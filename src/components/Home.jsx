import React from "react";
import styled from "styled-components";

import Banner from "./Banner";
import ItemList from "./ItemList";

const Container = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  padding: 2rem;
`;

function Home() {
  return (
    <Container>
      <Banner />
      <ItemList />
    </Container>
  );
}

export default Home;
