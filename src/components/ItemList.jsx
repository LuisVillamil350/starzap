import React from "react";
import styled from "styled-components";

import Item from "./Item";
import { device } from "../utils/mobile";

const Container = styled.div`
  margin-top: 5rem;
  display: flex;
  justify-content: center;
  width: 100%;
  height: 100%;
  flex-wrap: wrap;

  > * {
    margin-left: 4rem;
    margin-right: 4rem;
    margin-bottom: 1rem;

    @media ${device.mobileS} {
      margin-left: 1rem;
      margin-right: 1rem;
    }

    @media ${device.tablet} {
      margin-left: 2rem;
      margin-right: 2rem;
    }

    @media ${device.laptop} {
    }
  }
`;

function ItemList() {
  return (
    <Container>
      <Item />
      <Item />
      <Item />
      <Item />
      <Item />
      <Item />
      <Item />
      <Item />
    </Container>
  );
}

export default ItemList;
