import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import styled, { keyframes, css } from "styled-components";
import { withRouter } from "react-router-dom";
import Cart from "../../assets/png/cart.png";
import LogoImage from "../../assets/svg/LogoLetters.svg";
import { device } from "../../utils/mobile";

const Container = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: sticky;
  top: 1rem;

  @media ${device.mobileS} {
    flex-direction: column;
  }

  @media ${device.mobileM} {
    flex-direction: column;
  }

  @media ${device.mobileL} {
    flex-direction: column;
  }

  @media ${device.tablet} {
    flex-direction: row;
  }
`;

const Logo = styled.img`
  width: 10rem;
  transition: all 300ms;
  ${({ hideHeader }) => {
    if (hideHeader) {
      return "opacity: 0; ";
    }
    return "";
  }}

  @media ${device.mobileS} {
    width: 13rem;
    margin-bottom: 2rem;
  }

  @media ${device.tablet} {
    width: 10rem;
    margin-bottom: 0;
  }
`;

const Options = styled.div`
  display: flex;
  transition: all 300ms;

  ${({ hideHeader }) => {
    if (hideHeader) {
      return "opacity: 0; ";
    }
    return "";
  }}
  > * {
    &:not(:first-child) {
      margin-left: 3rem;
      @media ${device.mobileS} {
        margin-left: 1.3rem;
      }

      @media ${device.mobileM} {
        margin-left: 1rem;
      }

      @media ${device.mobileL} {
        margin-left: 3rem;
      }

      @media ${device.tablet} {
        margin-left: 3rem;
      }
    }
  }
`;

const Option = styled.button`
  font-size: 5rem;
  transition: all 0.2s;
  cursor: pointer;
  outline: 0;
  border: 0;
  background-color: transparent;
  position: relative;

  &:hover {
    transform: translateY(-3px);
  }

  &:active {
    transform: translateY(-1px);
  }

  @media ${device.mobileS} {
    font-size: 0.9rem;
  }

  @media ${device.mobileM} {
    font-size: 1rem;
  }

  @media ${device.mobileL} {
    font-size: 1rem;
  }

  @media ${device.tablet} {
    font-size: 1.2rem;
  }
`;

const CartContainer = styled.div`
  display: flex;
  height: 24px;
  transition: all 0.2s;
  align-self: flex-end;
  cursor: pointer;
  position: sticky;
  top: 0.5rem;

  ${({ hideHeader }) => (hideHeader ? "margin-left: auto;" : "")}

  &:hover {
    transform: translateY(-3px);
  }

  &:active {
    transform: translateY(-1px);
  }

  @media ${device.mobileS} {
    margin-top: 1rem;
  }

  @media ${device.mobileM} {
    margin-top: 1rem;
  }

  @media ${device.mobileL} {
    margin-top: 1rem;
  }

  @media ${device.tablet} {
    margin-top: 0;
  }
`;

const CartLogo = styled.img`
  height: 24px;
  width: 24px;
`;

const animateText = keyframes`
  0% {
    transform: translateY(-3px);
  }
  80% {
    transform: translateY(-1px);
  }
  100% {
    transform: translateY(0);
  }
`;

const animationCartCounter = css`
  animation: ${animateText} 0.3s linear;
`;

const CartCounterText = styled.div`
  font-size: 1rem;
  color: #008843;
  left: 1.3rem;
  top: 0.1rem;
  padding: 3px 6px;

  &:after {
    background-color: red;
  }

  ${({ newItemAdded }) => (newItemAdded ? animationCartCounter : "")}
`;

function getDocHeight() {
  let D = document;
  return Math.max(
    D.body.scrollHeight,
    D.documentElement.scrollHeight,
    D.body.offsetHeight,
    D.documentElement.offsetHeight,
    D.body.clientHeight,
    D.documentElement.clientHeight
  );
}

function Header({ cart, history }) {
  const [hideHeader, setHideHeader] = useState(false);
  const [newItemAdded, setNewItemAdded] = useState(false);

  function handleScroll(event) {
    let winheight =
      window.innerHeight ||
      (document.documentElement || document.body).clientHeight;
    let winWidth =
      window.innerWidth ||
      (document.documentElement || document.body).clientWidth;
    let docheight = getDocHeight();
    let scrollTop =
      window.pageYOffset ||
      (document.documentElement || document.body.parentNode || document.body)
        .scrollTop;
    let trackLength = docheight - winheight;
    let pctScrolled = Math.floor((scrollTop / trackLength) * 100);
    if (pctScrolled > 10 && winWidth >= 768) {
      setHideHeader(true);
    } else if (pctScrolled >= 4) {
      setHideHeader(true);
    } else {
      setHideHeader(false);
    }
  }

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  useEffect(() => {
    return () => {
      setNewItemAdded(true);
      setTimeout(() => {
        setNewItemAdded(false);
      }, 300);
    };
  }, [cart]);

  return (
    <Container>
      <Logo src={LogoImage} alt="StartZap" hideHeader={hideHeader} />

      <Options hideHeader={hideHeader}>
        <Option onClick={() => history.push("/")}>Home</Option>
        <Option>Comprar</Option>
        <Option>Seguimiento</Option>
        <Option>Contacto</Option>
      </Options>

      <CartContainer
        hideHeader={hideHeader}
        onClick={() => history.push("/cart")}
      >
        <CartLogo src={Cart} alt="Cart" />
        <CartCounterText newItemAdded={newItemAdded}>
          {cart.items.length}
        </CartCounterText>
      </CartContainer>
    </Container>
  );
}

const HeaderWithRouter = withRouter(Header);

export default connect(({ cart }) => ({
  cart
}))(HeaderWithRouter);
