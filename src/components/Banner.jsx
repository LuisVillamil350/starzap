import React from "react";
import styled, { keyframes } from "styled-components";

import BannerImageSRC from "../assets/png/banner.jpg";
import { device } from "../utils/mobile";

const moveInLeft = keyframes`
    0% {
        opacity: 0;
        transform: translateX(-100px);
    }

    80% { transform: translateX(10px) }

    100% {
        opacity: 1;
        transform: translate(0);
    }
`;

const moveInRight = keyframes`
    0% {
        opacity: 0;
        transform: translateX(100px);
    }

    80% { transform: translateX(-10px) }

    100% {
        opacity: 1;
        transform: translate(0);
}
`;

const Container = styled.div`
  margin-top: 5rem;
  display: flex;
  justify-content: center;
`;

const ImageContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  width: 100%;
`;

const BannerImage = styled.img`
  max-width: 300px;
  width: 100%;
  height: auto;
  justify-content: center;
`;

const TextContainer = styled.div`
  width: 100%;
  margin-top: 5rem;
  justify-content: center;
`;

const Title = styled.div`
  width: 100%;
  text-align: right;
  font-size: 1.5rem;
  font-weight: bold;
  color: #2e2e2e;

  @media ${device.mobileS} {
    font-size: 1rem;
  }

  @media ${device.mobileM} {
  }

  @media ${device.mobileL} {
  }

  @media ${device.tablet} {
    font-size: 1.5rem;
  }

  animation: ${moveInLeft} 1s ease-in;
`;

const SubTitle = styled.div`
  width: 100%;
  text-align: right;
  font-size: 1rem;
  font-weight: normal;
  color: #009688;

  @media ${device.mobileS} {
    font-size: 0.8rem;
  }

  @media ${device.mobileM} {
  }

  @media ${device.mobileL} {
  }

  @media ${device.tablet} {
    font-size: 1rem;
  }

  animation: ${moveInRight} 1s ease-in;
`;

function Banner() {
  return (
    <Container>
      <TextContainer>
        <Title>Playeras para la comunidad</Title>
        <SubTitle>Solo en Mexico</SubTitle>
      </TextContainer>
      <ImageContainer>
        <BannerImage src={BannerImageSRC} alt="Banner" />
      </ImageContainer>
    </Container>
  );
}

export default Banner;
