import React from "react";
import { connect } from "react-redux";
import styled, { keyframes } from "styled-components";
import PlaceHolder from "../assets/png/placeholder.jpg";
import { addToCart } from "../store/actions/cart.actions";

const ImageContainer = styled.img`
  width: 14rem;
  height: 12rem;
`;

const DetailsContainer = styled.div`
  display: flex;
  flex-grow: 1;
  height: 100%;
  width: 100%;
  margin-top: 1rem;
  flex-direction: column;

  > * {
    margin-top: 0.5rem;
  }
`;

const ItemTitle = styled.div`
  font-weight: bold;
`;

const ItemPrice = styled.div`
  color: gray;
`;

const AddToCart = styled.button`
  color: #ff3368;
  font-weight: bold;
  cursor: pointer;
  font-size: 1.2rem;
  letter-spacing: 0.2rem;
  transition: all 0.2s;
  user-select: none;
  padding: 1rem;
  background-color: transparent;
  border: 0;
  outline: 0;

  &:hover {
    transform: translateY(-3px);
  }

  &:active {
    transform: translateY(-1px);
  }
`;

const Container = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  height: 20rem;
  width: 14rem;
  padding: 1rem;
  transition: all 0.2s;

  &:hover {
    transform: translateY(-3px);
    box-shadow: 0 10px 20px rgba(0, 0, 0, 0.2);
  }
`;

function Item({ addToCart }) {
  const item = {
    id: "afasdaweafaedwae",
    title: "ready to serve",
    price: 250
  };
  return (
    <Container>
      <ImageContainer src={PlaceHolder} alt="product name" />
      <DetailsContainer>
        <ItemTitle>Ready to serve</ItemTitle>
        <ItemPrice>$250.00</ItemPrice>
        <AddToCart onClick={() => addToCart(item.id)}>+ AGREGAR</AddToCart>
      </DetailsContainer>
    </Container>
  );
}

export default connect(
  null,
  { addToCart }
)(Item);
