import React from "react";
import styled from "styled-components";
import { BrowserRouter as Router, Route } from "react-router-dom";

import Header from "./components/layout/Header";
import Home from "./components/Home";
import Cart from "./components/Cart";
import { device, size } from "./utils/mobile";

const Page = styled.div`
  display: flex;
  flex-direction: column;
  flex-grow: 1;

  /* padding: 1rem 3rem; */

  font-family: "Roboto", sans-serif;

  @media ${device.mobileS} {
    max-width: ${size.mobileS};
  }

  @media ${device.mobileM} {
    max-width: ${size.mobileM};
  }

  @media ${device.mobileL} {
    max-width: ${size.mobileL};
  }

  @media ${device.tablet} {
    max-width: ${size.tablet};
  }

  @media ${device.laptop} {
    max-width: 1440px;
    margin: auto;
  }
`;

function App() {
  return (
    <Page>
      <Router>
        <Header />
        <Route exact path="/" component={Home} />
        <Route exact path="/cart" component={Cart} />
      </Router>
    </Page>
  );
}

export default App;
