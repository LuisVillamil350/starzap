import { ADD_TO_CART_SUCCESS } from "../actions/cart.actions";

const initialState = {
  items: []
};

function CartReducer(state = initialState, action) {
  switch (action.type) {
    case ADD_TO_CART_SUCCESS:
      return {
        ...state,
        items: [...state.items, action.payload]
      };
    default:
      return state;
  }
}

export default CartReducer;
