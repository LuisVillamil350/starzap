export const ADD_TO_CART_START = "ADD_TO_CART_START";
export const ADD_TO_CART_SUCCESS = "ADD_TO_CART_SUCCESS";

export function addToCart(itemId) {
  return {
    type: ADD_TO_CART_START,
    payload: itemId
  };
}
