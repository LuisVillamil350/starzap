import { takeEvery, put } from "redux-saga/effects";

import {
  ADD_TO_CART_SUCCESS,
  ADD_TO_CART_START
} from "../actions/cart.actions";

export function* addToCart({ payload }) {
  yield put({ type: ADD_TO_CART_SUCCESS, payload });
}

export function* cartSaga() {
  yield takeEvery(ADD_TO_CART_START, addToCart);
}
