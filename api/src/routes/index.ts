import { Router } from "express";

interface IRoute {
  path: string;
  handler: Router;
  middleware: any[];
}

export const routes: IRoute[] = [
  {
    path: "/",
    handler: Router().get("/", (req, res) => res.json("hey")),
    middleware: []
  }
];
