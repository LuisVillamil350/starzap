import cors from "cors";
import express from "express";
import helmet from "helmet";
import http from "http";
import { Connection } from "typeorm";

import { routes } from "./routes";
import { connectDB } from "./db";

export class Server {
  private readonly app: express.Application;
  private readonly server: http.Server | undefined;
  private connection: Connection | undefined;

  constructor() {
    this.app = express();
    this.server = http.createServer(this.app);
  }

  public async start(): Promise<http.Server | undefined> {
    try {
      this.connection = await connectDB();
      console.log("Connected to database");
      this.initExpressApp();

      for (const route of routes) {
        this.app.use(route.path, route.middleware, route.handler);
      }
      return this.server;
    } catch (error) {
      throw error;
    }
  }

  private initExpressApp(): void {
    this.app.use(helmet());
    this.app.use(
      cors({
        methods: ["GET", "POST", "PUT", "DELETE"],
        allowedHeaders: ["Content-Type"]
      })
    );
    this.app.use(express.urlencoded({ extended: false }));
  }
}
