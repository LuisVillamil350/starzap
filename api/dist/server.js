"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cors_1 = __importDefault(require("cors"));
const express_1 = __importDefault(require("express"));
const helmet_1 = __importDefault(require("helmet"));
const http_1 = __importDefault(require("http"));
const routes_1 = require("./routes");
const db_1 = require("./db");
class Server {
    constructor() {
        this.app = express_1.default();
        this.server = http_1.default.createServer(this.app);
    }
    start() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                this.connection = yield db_1.connectDB();
                console.log("Connected to database");
                this.initExpressApp();
                for (const route of routes_1.routes) {
                    this.app.use(route.path, route.middleware, route.handler);
                }
                return this.server;
            }
            catch (error) {
                throw error;
            }
        });
    }
    initExpressApp() {
        this.app.use(helmet_1.default());
        this.app.use(cors_1.default({
            methods: ["GET", "POST", "PUT", "DELETE"],
            allowedHeaders: ["Content-Type"]
        }));
        this.app.use(express_1.default.urlencoded({ extended: false }));
    }
}
exports.Server = Server;
//# sourceMappingURL=server.js.map