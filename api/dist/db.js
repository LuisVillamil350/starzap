"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const config_1 = require("./config");
function connectDB() {
    return __awaiter(this, void 0, void 0, function* () {
        const options = {
            type: "mysql",
            host: config_1.CONFIG.DB_HOST,
            port: config_1.CONFIG.DB_PORT,
            username: config_1.CONFIG.DB_USER,
            password: config_1.CONFIG.DB_PASSWORD,
            database: config_1.CONFIG.DB_NAME,
            entities: [`${__dirname}/../models/**{.ts,.js}`],
            synchronize: config_1.CONFIG.DB_SYNC,
            cli: {
                entitiesDir: `${__dirname}/../models`
            }
        };
        return yield typeorm_1.createConnection(options);
    });
}
exports.connectDB = connectDB;
//# sourceMappingURL=db.js.map